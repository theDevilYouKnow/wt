<?
declare(strict_types=1);

/**
 * Данная папка не предназначена для публичного доступка,
 * однако в данном проекте к ней есть доступ из вне.
 */

header('HTTP/1.1 301 Moved Temporarily');
header('Location:/chat/public/');