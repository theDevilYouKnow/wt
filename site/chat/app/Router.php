<?
declare(strict_types=1);

namespace app;

use app\models\Session;
use app\models\User;
use app\controllers as cs;

class Router {

	const COOKIE_BANNED = 'b_token';


	static function redirect (string $uri) {
		header('HTTP/1.1 302 Moved Temporarily');
		header('Location:/chat/public/' . $uri);
	}

	static function start () {
		$uri = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
		preg_match('/^chat\/public\/(.*)$/', $uri, $_);
		$uri = (string)$_[1];

		/**
		 * @var $User User
		 * @var $Session Session
		 */
		list($User, $Session) = cs\Base::_user();

		if (self::isIpBanned($User, $Session)) {
			// чтобы забаненный пользователь не отобразился в зашедшие в чат (с запасом в 10 сек)
			$Session->upLastTime(time() - cs\Base::USER_ONLINE_TIME_OFFSET - 10);
			return;
		}

		if ('debug' === $uri)
			cs\Other::debug();
		else if ('logout' === $uri)
			cs\Auth::logout();

		else if ('' === $uri)
			cs\Chat::rend($User, (int)$_GET['delphi_client_version']);

		else if ('api/getData' === $uri)
			cs\Chat::jGetData($User, (int)$_POST['last_message_id']);

		else if ('api/addMessage' === $uri)
			cs\Chat::jAddMessage($User, (string)$_POST['text'],
				$_POST['receivers_ids'] ? explode(',', $_POST['receivers_ids']) : [], (int)$_POST['last_message_id']);
		else if ('api/generateCoin' === $uri)
			cs\Chat::jGenerateCoin($User, (int)$_POST['last_message_id']);
		else if ('api/generateTowns' === $uri)
			cs\Chat::jGenerateTowns($User, (int)$_POST['last_message_id']);
		else if ('api/generateTemplate' === $uri)
			cs\Chat::jGenerateTemplate($User, (int)$_POST['last_message_id']);
		else if ('api/setIgnoreUser' === $uri)
			cs\Chat::jSetIgnoreUser($User, (int)$_POST['ignore_user_id'], (bool)$_POST['is_ignore'],
				(int)$_POST['last_message_id']);

		else if ('api/auth' === $uri)
			cs\Auth::jAuth((string)$_POST['login'], (string)$_POST['password']);

		else
			self::redirect('');
	}

	private static function isIpBanned (User $User = NULL, Session $Session = NULL) : bool {
		$bannedIds = [
			// WLK
			5976,
			5971,
			5973,
			253,
		];

		if ($_COOKIE[self::COOKIE_BANNED]) {
			cs\Base::_report('Входа с забаненнымим куками, пользователь ' . ($User ? $User->username : '--'));
			return TRUE;
		}

		if ($User AND in_array($User->user_id, $bannedIds, TRUE)) {
			$bToken = $Session ? $Session->token : '-1';
			setcookie(self::COOKIE_BANNED, $bToken, 9999999 + time(), '/', 'heroeswt.net', FALSE, FALSE);
			cs\Base::_report("Вход с забаненным id, пользователь {$User->username}");
			return TRUE;
		}

		return FALSE;
	}
}