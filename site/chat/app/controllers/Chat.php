<?
declare(strict_types=1);

namespace app\controllers;

use app\models\Session;
use app\models\User;
use app\models\UserExtend;
use app\models\Message;

use zPHP\API;

class Chat extends Base {

	static function rend (User $User = NULL, int $delphiClientVersion = 0) {
		$_isGuest        = $User ? FALSE : TRUE;
		$_isDelphiClient = $delphiClientVersion > 0;
		if ($_GET['test'])
			include BASE_DIR . '/app/views/chat_test.php';
		else
			include BASE_DIR . '/app/views/chat.php';

		if ($delphiClientVersion AND ($Session = self::_getSession()))
			$Session->upDelphiClientVersion($delphiClientVersion);
	}

	static function jGetData (User $User = NULL, int $lastMessageId) {
		return API\Rend::success(self::prepJData($User, $lastMessageId));
	}

	static function jAddMessage (User $User = NULL, string $text, array $receiversIds, int $lastMessageId) {
		if (!$User)
			return API\Rend::error('Необходимо войти', self::ERROR_CODE_RUNTIME);

		$receiversIds = array_map('intval', $receiversIds);

		$text = mb_substr(trim($text), 0, 5000);
		if (!$text)
			return API\Rend::error('Текст не должен быть пустым', self::ERROR_CODE_RUNTIME);

		// Проверка что переданные id существуют
		$prepReceiversIds = [];
		foreach (User::getByIds($receiversIds) as $Receiver)
			$prepReceiversIds[] = $Receiver->user_id;
		$prepReceiversIds = array_unique($prepReceiversIds);

		if ($User->user_inactive_time) {
			if (count($prepReceiversIds) != 1)
				return API\Rend::error('Вам запрещено отправлять публичные сообщения', self::ERROR_CODE_RUNTIME);
		}

		Message::add($User, $text, $prepReceiversIds);

		return API\Rend::success(self::prepJData($User, $lastMessageId));
	}

	static function jGenerateCoin (User $User = NULL, int $lastMessageId) {
		if (!$User)
			return API\Rend::error('Необходимо войти', self::ERROR_CODE_RUNTIME);

		if (!$SystemUser = User::getById(User::USER_ID_SYSTEM_CHAT)) {
			self::_report((new \Exception('Не найден системный пользователь'))->__toString());
			return API\Rend::error('Ошибка сервера', self::ERROR_CODE_RUNTIME);
		}

		$text = $User->username . ' generate coin - ' . mt_rand(0, 1);

		Message::add($SystemUser, $text, []);

		return API\Rend::success(self::prepJData($User, $lastMessageId));
	}

	static function jGenerateTowns (User $User = NULL, int $lastMessageId) {
		if (!$User)
			return API\Rend::error('Необходимо войти', self::ERROR_CODE_RUNTIME);

		if (!$SystemUser = User::getById(User::USER_ID_SYSTEM_CHAT)) {
			self::_report((new \Exception('Не найден системный пользователь'))->__toString());
			return API\Rend::error('Ошибка сервера', self::ERROR_CODE_RUNTIME);
		}

		$towns = ['Castle', 'Rampart', 'Tower', 'Necropolis', 'Dungeon', 'Inferno', 'Fortress', 'Citadel', 'Conflux'];
		$t1    = $towns[array_rand($towns, 1)];
		$t2    = $towns[array_rand($towns, 1)];
		$text  = $User->username . ' generate towns - (' . $t1 . ' vs ' . $t2 . ')';

		Message::add($SystemUser, $text, []);

		return API\Rend::success(self::prepJData($User, $lastMessageId));
	}

	static function jGenerateTemplate (User $User = NULL, int $lastMessageId) {
		if (!$User)
			return API\Rend::error('Необходимо войти', self::ERROR_CODE_RUNTIME);

		if (!$SystemUser = User::getById(User::USER_ID_SYSTEM_CHAT)) {
			self::_report((new \Exception('Не найден системный пользователь'))->__toString());
			return API\Rend::error('Ошибка сервера', self::ERROR_CODE_RUNTIME);
		}

		$list = [
			'8mm6а',
			'6LM10a',
			'8ХМ12а',
			'Nostalgia',
			'Spider',
			'Mini-Nostalgia',
			'Panic',
			'2SM4d(3)',
			'Balance',
			'Jebus Cross',
			'HyperCube',
			'h3dm1',
			'hjeb4'
		];
		$text = $User->username . ' generate template - ' . $list[array_rand($list, 1)];

		Message::add($SystemUser, $text, []);

		return API\Rend::success(self::prepJData($User, $lastMessageId));
	}

	static function jSetIgnoreUser (User $CurUser = NULL, int $ignoreUserId, bool $isIgnore, int $lastMessageId) {
		if (!$CurUser)
			return API\Rend::error('Необходимо войти', self::ERROR_CODE_RUNTIME);

		if (!$IgnoredUser = User::getById($ignoreUserId)) {
			self::_report((new \Exception("Пользователь #$ignoreUserId не найден"))->__toString());
			return API\Rend::error('Пользователь не найден', self::ERROR_CODE_RUNTIME);
		}

		// Запрещенные для игнора юзеры (системный и себя)
		$exIds = [User::USER_ID_SYSTEM_CHAT, $CurUser->user_id];
		if (!in_array($IgnoredUser->user_id, $exIds)) {
			$CurUserExtend = UserExtend::getAdd($CurUser);
			$CurUserExtend->setIgnoredUser($IgnoredUser, $isIgnore);
		}

		return API\Rend::success(self::prepJData($CurUser, $lastMessageId));
	}


	/** @throws \PDOException */
	private static function prepJData (User $CurUser = NULL, int $lastMessageId) : array {
		$CurUserExtend = $CurUser ? UserExtend::getAdd($CurUser) : NULL;
		$playSound     = FALSE;

		/////////////////////////////////////////////////////////////
		// Сбор данных по необходимым пользовалям для единого запроса

		$messages     = Message::getAfterId($lastMessageId ?: Message::getLastId() - 100, 300);
		$lastSessions = Session::getByLastTime(self::USER_ONLINE_TIME_OFFSET);
		usort($lastSessions, function (Session $a, Session $b) {
			return $a->user_id <=> $b->user_id;
		});

		$usersIds = [];
		foreach ($messages as $Message) {
			$usersIds[] = $Message->user_id;
			$usersIds   = array_merge($usersIds, $Message->getReceiversIdsArray()); // получатели приватных сообщений
		}
		foreach ($lastSessions as $Session)
			$usersIds[] = $Session->user_id;

		/** @var User[] $usersMap [`user id` => User] */
		$usersMap = [];
		foreach (User::getByIds(array_unique($usersIds)) as $User)
			$usersMap[$User->user_id] = $User;


		//////////////////////////////
		// Массив пользователей онлайн

		$jUsers              = [];
		$lastSessionsUserIds = [];
		foreach ($lastSessions as $Session) {
			if (!$User = $usersMap[$Session->user_id])
				continue; // TODO report
			if (in_array($User->user_id, $lastSessionsUserIds))
				continue;
			$lastSessionsUserIds[] = $User->user_id;
			$jUsers[]              = self::prepJUser($User,
				$CurUserExtend ? $CurUserExtend->isIgnoredUser($User) : FALSE);
		}


		///////////////////
		// Массив сообщений

		$jMessages = [];
		foreach ($messages as $Message) {
			if (!$User = $usersMap[$Message->user_id]) {
				self::_report((new \Exception("Пользователь #{$Message->user_id} не найден"))->__toString());
				continue;
			}

			// Пропускаем если юзер в игноре
			if ($CurUserExtend AND $CurUserExtend->isIgnoredUser($User))
				continue;

			// Звуковое сообщени если твой логин есть в тексте сообщения и чат
			// и это не первая загрузка сообщений ($lastMessageId болья нуля)
			if ($lastMessageId AND $CurUser AND (strpos($Message->text, $CurUser->username) !== FALSE))
				$playSound = TRUE;

			/////////////////////////////////////
			// Если у сообщения заданы получатели

			$receiversIds = $Message->getReceiversIdsArray();
			/** @var User[] $receivers */
			$receivers = [];
			if ($receiversIds) {
				// Гостям никаких сообщений, но это плохо работает когда отправляют всем кроме
				// Данная проверка также исключает использование nullable $CurUser в дальнейшей логике
				if (!$CurUser)
					continue;

				if ($CurUser->user_id === $Message->user_id) { // свои сообщения показывать всегда
				}
				else if (!in_array($CurUser->user_id, $receiversIds, TRUE))
					continue;

				// Звуковое уведомление при личном сообщении соблюдении условий:
				// - не первая загрузка сообщений ($lastMessageId болья нуля)
				// - текущий пользователь есть в списке получателей
				// - список получателей не больше 3х
				if ($lastMessageId AND in_array($CurUser->user_id, $receiversIds, TRUE) AND (count($receivers) <= 3))
					$playSound = TRUE;

				foreach ($receiversIds as $receiverId) {
					if (!$Receiver = $usersMap[$receiverId])
						continue;
					$receivers[] = $Receiver;
				}
			}

			///////////////////////
			// Формирование подписи

			$note      = '';
			$noteColor = 'red';
			if ($receivers) {
				$exceptedIds = array_flip($lastSessionsUserIds);
				unset($exceptedIds[$User->user_id]); // иначе будет упоминание отправителя
				foreach ($receivers as $Receiver)
					unset($exceptedIds[$Receiver->user_id]);
				$exceptedIds = array_flip($exceptedIds);
				/** @var User[] $exceptedUsers */
				$exceptedUsers = [];
				foreach ($exceptedIds as $exceptedId)
					if ($UserExcept = $usersMap[$exceptedId])
						$exceptedUsers[] = $UserExcept;

				if ((count($exceptedUsers) > 0) AND (count($exceptedUsers) < count($receivers))) {
					$names = [];
					foreach ($exceptedUsers as $Receiver)
						$names[] = $Receiver->username;
					$note      = '[toAll except ' . implode(', ', $names) . ']';
					$noteColor = 'green';
				}
				else {
					$names = [];
					foreach ($receivers as $Receiver)
						$names[] = $Receiver->username;
					$note = '[to ' . implode(', ', $names) . ']';
				}
			}

			$jMessages[] = self::prepJMessage($Message, $User, $note, $noteColor);
		}

		return [
			'messages'   => array_reverse($jMessages),
			'play_sound' => $playSound,
			'users'      => $jUsers,
			'cur_user'   => $CurUser ? self::prepJUser($CurUser, FALSE) : NULL
		];
	}

	private static function prepJMessage (Message $Message, User $User, string $note, string $noteColor) : array {
		$note = $note ? "<i style='color:$noteColor;'>$note</i>" : '';

		$userName = "<b class='username'>{$User->username}</b>: ";
		if ($User->user_id === User::USER_ID_SYSTEM_CHAT)
			$userName = '';

		$text = "{$userName}$note " . strip_tags($Message->text);
		return [
			'id'   => $Message->id,
			// TODO is_delphi_client
			'text' => nl2br(self::changeTextToLink($text, TRUE)),
			'time' => $Message->time
		];
	}

	private static function prepJUser (User $User, bool $isIgnored) : array {
		return [
			'id'         => $User->user_id,
			'login'      => $User->username,
			'is_ignored' => $isIgnored
		];
	}

	private static function changeTextToLink (string $text, bool $isDelphiClient) : string {
		$htmlTarget = $isDelphiClient ? '' : '_blank';

		$text = preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is",
			"$1$2<a href=\"http://$3\" target='$htmlTarget' >$3</a>", $text);
		$text = preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is",
			"$1$2<a href=\"$3\" target='$htmlTarget'>$3</a>", $text);
		return $text;
	}
}