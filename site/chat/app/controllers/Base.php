<?
declare(strict_types=1);

namespace app\controllers;

use app\Config;
use app\models\User;
use app\models\Session;

use zPHP\Telegram;

class Base {

	const USER_ONLINE_TIME_OFFSET = 20; // секунды

	const COOKIE_TOKEN = 'user_token';

	const ERROR_CODE_RUNTIME = 'runtime_error';
	const ERROR_CODE_SERVER  = 'server_error';

	static function _report (string $message) {
		$url      = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		$post     = $_POST ? "\npost: " . json_encode($_POST) : '';
		$httpInfo = $url . "\nip: {$_SERVER['REMOTE_ADDR']}" . $post;
		Telegram\Core::sendMessage(Config::TG_TOKEN_LOGS, Config::TG_CHAT_ADMIN, "$httpInfo\n\n$message");
	}


	/**
	 * @throws \PDOException
	 * @return [User|null, Session|null]
	 */
	static function _user () : array {
		if (!$Session = self::_getSession())
			return [NULL, NULL];

		$Session->upLastTime(time());
		$curIp = $_SERVER['REMOTE_ADDR'];
		if ($Session->ip !== $curIp)
			$Session->upIp($curIp);

		if (!$User = User::getById($Session->user_id)) {
			self::_report((new \Exception("Пользователь #{$Session->user_id} не найден"))->__toString());
			return [NULL, NULL];
		}

		self::setAuthData($_COOKIE[self::COOKIE_TOKEN]);
		return [$User, $Session];
	}

	/**
	 * @throws \PDOException
	 * @return Session|null
	 */
	static function _getSession () {
		// Проверка существования cookie
		if (!$cookie = $_COOKIE[self::COOKIE_TOKEN])
			return NULL;

		if (!$Session = Session::getByToken($cookie)) {
			self::_report((new \Exception("Кабинет с токеном $cookie не найден"))->__toString());
			return NULL;
		}

		return $Session;
	}

	static function _auth (Session $Session) {
		self::setAuthData($Session->token);
	}

	static function _logout () {
		setcookie(self::COOKIE_TOKEN, '', time() - 999999, '/', 'heroeswt.net', FALSE, FALSE);
	}

	private static function setAuthData (string $session) {
		setcookie(self::COOKIE_TOKEN, $session, 9999999 + time(), '/', 'heroeswt.net', FALSE, FALSE);
	}
}