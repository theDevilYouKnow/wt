<?
declare(strict_types=1);

namespace app\controllers;

use app\Router;
use app\models\User;
use app\models\Session;

use zPHP\API;

class Auth extends Base {

	static function logout () {
		self::_logout();
		if ($Session = self::_getSession())
			$Session->upLastTime(time() - self::USER_ONLINE_TIME_OFFSET);
		Router::redirect('');
	}

	static function jAuth (string $login, string $password) {
		if (!$User = User::getByLogin($login))
			return API\Rend::error("Пользователь $login не зарегистрирован", self::ERROR_CODE_RUNTIME);

		$res = file_get_contents('http://heroeswt.net/chat/public/check_phpbb_hash.php', FALSE, stream_context_create([
			'http' => [
				'ignore_errors' => TRUE,
				'method'        => 'POST',
				'header'        => 'Content-Type: application/x-www-form-urlencoded' . PHP_EOL,
				'content'       => http_build_query([
					'password' => $password,
					'hash'     => $User->user_password
				])
			]
		]));

		if (((string)$res) !== '1')
			return API\Rend::error("Пароль не верный", self::ERROR_CODE_RUNTIME);

		$Session = Session::addGet($User);
		self::_auth($Session);

		return API\Rend::success([]);
	}
}