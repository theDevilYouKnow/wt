<html>

<head>
    <title>WT чат</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://yastatic.net/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <base href="/chat/public/">
    <style>
        html, body {
            height : 100%;
        }

        * {
            font-family : Tahoma;
            font-size   : 12px;
            color       : #000;
        }

        #users > *:nth-child(odd) {
            background : #efefef;
        }

        .username {
            cursor : pointer;
            color  : blue;
        }

        .username:hover {
            text-decoration : underline;
        }

        @media (max-width : 500px) {
            #right-side {
                display : none;
            }
        }

        #add-message-form button {
            margin-top    : 4px;
            border-radius : 1px;
            padding       : 3px 8px;
            font-size     : 11px;
            box-shadow    : 1px 1px 0 #ccc;
        }

        .nav-height {
            height : 50px;
        }
    </style>
</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top"
     style="box-shadow:0 0 5px #eee;">
    <div style="max-width:1200px; margin:0 auto;">

        <div class="navbar-header">

            <button type="button"
                    class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1"
                    aria-expanded="false">

                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand"
               href="#">Heroes 3 World Tournament</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav">

                <li><a href="/" target="<?= $_isDelphiClient? '' : '_blank' ?>">О проекте</a></li>

                <li><a href="/forum" target="<?= $_isDelphiClient? '' : '_blank' ?>">Форум</a></li>

                <li><a href="/howtoplay.php" target="<?= $_isDelphiClient? '' : '_blank' ?>">Как начать играть</a></li>

                <li><a href="/rules.php" target="<?= $_isDelphiClient? '' : '_blank' ?>">Правила</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right"
                style="margin-right:0;">

				<? if (!$_isGuest) { ?>

                    <li class="dropdown">

                        <a href="#"
                           class="dropdown-toggle"
                           data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false">

                            <span id="navbar-login">загрузка..</span>

                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="javascript:logout();">Выйти</a></li>
                        </ul>
                    </li>

				<? } ?>
            </ul>
        </div>
    </div>
</nav>

<table style="max-width:1200px; width:100%; height:100%; margin:0 auto;">

    <tr>

        <td style="padding:0 15px; word-wrap:break-word; height:100%;">

            <table style="height:100%; width:100%;">

                <!-- для отступа navbar -->
                <tr class="nav-height">
                    <td></td>
                </tr>

                <tr>
                    <td id="messages_wrapper" style="height: 100%;">
                        <div style="position:relative; height:100%; width:100%;">
                            <div id="messages"
                                 style="height:100%; width:100%; overflow-y:scroll; padding-bottom:5px; position:absolute; top:0;"></div>
                        </div>
                    </td>
                </tr>

                <tr style="height:0;" id="messages_send_form_wrapper">
                    <td>
						<? if ($_isGuest) { ?>
                            <div style="padding:25px 0 35px 0; text-align:center;">
                                Для отправки сообщений

                                <a href="#"
                                   data-toggle="modal"
                                   data-target="#modal-auth">войдите</a>

                                или

                                <a href="/forum/ucp.php?mode=register"
                                   target="<?= $_isDelphiClient? '' : '_blank' ?>">зарегистрируйтесь</a>.
                            </div>

						<? }
						else { ?>
                            <form onsubmit="Chat.addMessage($(this.button_send_message), $(this.text)); return false;"
                                  id="add-message-form"
                                  style="margin-bottom:0; padding-bottom:8px;">

                            <textarea name="text"
                                      class="form-control input-sm"
                                      placeholder="..."
                                      style="height:50px; border-radius:1px;"
                                      id="add-message-form__text"
                                      required
                                      autofocus></textarea>

                                <button class="btn btn-sm btn-primary"
                                        type="submit"
                                        name="button_send_message"
                                        data-loading-text="Отправка..">
                                    Отправить
                                </button>

                                <button class="btn btn-sm btn-default"
                                        type="button"
                                        onclick="Chat.generateCoin($(this));"
                                        data-loading-text="Генерация..">
                                    Монетка [0;1]
                                </button>

                                <button class="btn btn-sm btn-default"
                                        type="button"
                                        onclick="Chat.generateTowns($(this));"
                                        data-loading-text="Генерация..">
                                    Случайные замки
                                </button>

                                <button class="btn btn-sm btn-default"
                                        type="button"
                                        onclick="Chat.generateTemplate($(this));"
                                        data-loading-text="Генерация..">
                                    Случайный шаблон
                                </button>

                                <button class="btn btn-sm btn-default"
                                        type="button"
                                        onclick="Chat.cleanChat();">
                                    Очистить
                                </button>
                            </form>
						<? } ?>
                    </td>
                </tr>

            </table>


        </td>

        <td id="right-side"
            style="width:150px; min-width:170px; padding-right:15px; vertical-align:top;">

            <div class="nav-height"></div>

            <div>
                <div style="display:flex; flex-direction:row; align-items:center; padding:6px 0;">

                    <input type="checkbox"
                           style="margin:0 0 0 5px;"
                           onclick="Chat.selectAllUsers($(this))">

                    <span class="glyphicon glyphicon-remove-circle"
                          style="margin:0 0 2px 4px; font-size:14px; color:#d40001;"></span>

                    <div style="margin:1px 0 0 5px; font-weight:900;"><span id="num_users"></span> players</div>
                </div>
                <div id="users"
                     style="overflow-y:auto;"></div>
            </div>
        </td>
    </tr>
</table>

<!-- Окно авторизации -->
<div class="modal fade" id="modal-auth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="width:300px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Вход</h4>
            </div>
            <div class="modal-body">

                <form onsubmit="return auth(this)"
                      style="margin-bottom:0;">

                    <input type="text"
                           placeholder="Логин"
                           name="login"
                           class="form-control input-sm"
                           style="margin-top:5px;"
                           required>

                    <input type="password"
                           placeholder="Пароль"
                           class="form-control input-sm"
                           name="password"
                           style="margin-top:15px;"
                           required>

                    <button type="submit"
                            data-loading-text="Загрузка.."
                            style="padding-left:30px; padding-right:30px; margin-top:15px; margin-bottom:5px; width:100%;"
                            class="btn btn-primary btn-sm"
                            name="button">
                        Войти
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Данный блок используется клиентом делфи -->
<!-- его изменение приводит к "миганию" клиента -->
<div id="last_play_sound_time"
     style="display:none;"></div>

<script src="https://yastatic.net/jquery/1.9.1/jquery.min.js"></script>
<script src="https://yastatic.net/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="/chat/public/js/ion_sound/ion.sound.min.js"></script>
<script src="js/chat.js?v43"></script>

<script>
    /**
     * Установка актуального размера для блока сообщений
     * проблемы были в основном в ie и ff, но для других
     * браузеров этот скрипт не нарушает работу.
     */
    function ieCompatUpMessagesHeight() {
        var docHeight = document.body.clientHeight;
        var hMessages = docHeight - ($('#messages_send_form_wrapper').height() + $('.nav-height').height());
        $('#messages_wrapper').height(hMessages);
    }

    $(window).resize(function () {
        ieCompatUpMessagesHeight();
    });

    $(document).ready(function () {
        ieCompatUpMessagesHeight()
    });
</script>

<script>
    var _isWindowActive;
    $(window).blur(function () {
        _isWindowActive = false;
    });
    $(window).focus(function () {
        _isWindowActive = true;
    });

    Chat.start();

    function logout() {
        if (!confirm('Уверены что хотите выйти?'))
            return;

        window.top.location.href = 'logout';
    }

    var $addMessageFormText = $('#add-message-form__text');

    $addMessageFormText
        .keydown(function (e) {
            if (e.keyCode === 13 && e.ctrlKey) {
                $(this).val(function (i, val) {
                    return val + "\n";
                });
            }
        }).keypress(function (e) {
        if (e.keyCode === 13 && !e.ctrlKey) {
            $(this.form).trigger('submit');
            return false;
        }
    });

    $(function () {
        $(document).on('click', '.username', function () {
            var curText = $addMessageFormText.val();
            $addMessageFormText
                .val(curText + $(this).text() + ', ')
                .focus();
        });
    });

    function auth(form) {
        var $btn = $(form.button);
        $btn.button('loading');

        var data = {
            login: form.login.value,
            password: form.password.value
        };
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'api/auth',
            data: data,
            success: function (j) {
                $btn.button('reset');

                if (j.status === 'error')
                    return alert(j.message);

                window.top.location.href = '/chat';
            },
            error: function () {
                $btn.button('reset');
                alert('Ошибка сервера')
            }
        });
        return false;
    }

    ion.sound({
        sounds: [
            {name: "snap"},
        ],
        path: "/chat/public/js/ion_sound/sounds/",
        preload: true,
        multiplay: true,
        volume: 0.9
    });

    function playSoundNewMessage() {
        ion.sound.play("snap");
    }
</script>
</body>

</html>