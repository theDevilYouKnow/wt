<?
declare(strict_types=1);

namespace app\models;

use app\Config;

class Base {

	/** @var \PDO|null */
	private static $PDO;

	/** @throws \PDOException */
	static function _pdo () : \PDO {
		if (!self::$PDO)
			self::$PDO = new \PDO('mysql:dbname=' . Config::MYSQL_DB . ';host=' . Config::MYSQL_HOST,
				Config::MYSQL_USER, Config::MYSQL_PASS, [
					\PDO::ATTR_EMULATE_PREPARES   => FALSE,
					\PDO::ATTR_STRINGIFY_FETCHES  => FALSE,
					\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
					\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION
				]);
		return self::$PDO;
	}
}