<?
declare(strict_types=1);

namespace app\models;

class UserExtend extends Base {

	/** @var int */
	public $user_id;

	/**
	 * Описание системы игноров
	 *
	 * Строка из id пользователей разделенными запятыми.
	 * Хранятся данные о игноре/разигноре пользователей данным пользователем.
	 * Игноренные пользователи - с положительными id, разыгноренные с отрицательными.
	 *
	 * Вопрос: для чего нужно хранить данные о разыгноренные пользователях?
	 * Ответ: по мимо системы игнора есть система глобальных банов системой,
	 *        пользователи под таким баном могут стать сообщения только тем
	 *        кто их разыгнорил, для этого и храним данные об разыгноре.
	 *
	 * @var string
	 */
	public $ignored_users_ids;

	/** @throws \PDOException */
	function setIgnoredUser (User $User, bool $isIgnored) : UserExtend {
		// Массив текущих игнорируемых
		$ids = [];
		if ($this->ignored_users_ids)
			$ids = explode(',', $this->ignored_users_ids);

		$userId    = $User->user_id;
		$userIdInv = $User->user_id * -1;

		// Удаление прежних данных
		$ids = array_flip($ids);
		unset($ids[$userId]);
		unset($ids[$userIdInv]);
		$ids = array_flip($ids);

		$ids[] = $isIgnored ? $userId : $userIdInv; // заигнорил/разигнорил

		$ids = array_unique($ids);
		$ids = array_map('intval', $ids);

		$this->ignored_users_ids = implode(',', $ids);

		$sql = 'UPDATE chat_users_extend SET ignored_users_ids=? WHERE user_id=?';
		$STM = self::_pdo()->prepare($sql);
		$STM->execute([$this->ignored_users_ids, $this->user_id]);

		return $this;
	}

	function isIgnoredUser (User $User) : bool {
		// Себя игнорить не нужно
		if ($this->user_id == $User->user_id)
			return FALSE;

		$ids = [];
		if ($this->ignored_users_ids)
			$ids = explode(',', $this->ignored_users_ids);

		// Если забанен проверка может разигнорен
		if ($User->user_inactive_time)
			return !in_array($User->user_id * -1, $ids);
		else
			return in_array($User->user_id, $ids);
	}

	/**
	 * @throws \PDOException
	 * @return UserExtend|null
	 */
	static function getByUser (User $User) {
		$sql = 'SELECT * FROM chat_users_extend WHERE user_id=?';
		$STM = self::_pdo()->prepare($sql);
		$STM->execute([$User->user_id]);
		return $STM->fetchObject(self::class) ?: NULL;
	}

	/**
	 * @throws \PDOException
	 * @throws \Exception
	 */
	static function getAdd (User $User) : UserExtend {
		try {
			self::_pdo()->beginTransaction();

			if ($UserExtend = self::getByUser($User)) {
				self::_pdo()->commit();
				return $UserExtend;
			}

			$sql = 'INSERT INTO chat_users_extend (user_id) VALUES (?)';
			self::_pdo()->prepare($sql)->execute([$User->user_id]);

			if (!$UserExtend = self::getByUser($User))
				throw new \Exception('Не удалось создать расширение для пользователя');

			self::_pdo()->commit();

			return $UserExtend;
		} catch (\Throwable $E) {
			self::_pdo()->rollBack();
			throw $E;
		}
	}
}