<?
declare(strict_types=1);

namespace app\models;

class Session extends Base {

	/** @var string */
	public $token;

	/** @var int */
	public $user_id;

	/** @var int */
	public $time_create;

	/** @var int */
	public $last_time;

	/** @var int */
	public $delphi_client_version;

	/** @var string|null */
	public $ip;


	/** @throws \PDOException */
	function upLastTime (int $time) {
		$sql = 'UPDATE chat_sessions SET last_time=? WHERE token=?';
		self::_pdo()->prepare($sql)->execute([$time, $this->token]);
		$this->last_time = $time;
		return $this;
	}

	/** @throws \PDOException */
	function upIp (string $ip) {
		$sql = 'UPDATE chat_sessions SET ip=? WHERE token=?';
		self::_pdo()->prepare($sql)->execute([$ip, $this->token]);
		$this->ip = $ip;
		return $this;
	}

	/** @throws \PDOException */
	function upDelphiClientVersion (int $isClient) {
		$sql = 'UPDATE chat_sessions SET delphi_client_version=? WHERE token=?';
		self::_pdo()->prepare($sql)->execute([$isClient, $this->token]);
		$this->delphi_client_version = $isClient;
		return $this;
	}

	/**
	 * @throws \PDOException
	 * @return Session|null
	 */
	static function getByToken (string $token) {
		$sql = 'SELECT * FROM chat_sessions WHERE token=?';
		$STM = self::_pdo()->prepare($sql);
		$STM->execute([$token]);
		return $STM->fetchObject(self::class) ?: NULL;
	}

	/**
	 * @throws \PDOException
	 * @throws \Exception
	 */
	static function addGet (User $User) : Session {
		try {
			self::_pdo()->beginTransaction();

			$token = bin2hex(random_bytes(15));

			$sql = 'INSERT INTO chat_sessions (token,user_id,time_create,last_time) VALUES (?,?,?,?)';
			self::_pdo()->prepare($sql)->execute([$token, $User->user_id, time(), time()]);

			if (!$Session = self::getByToken($token))
				throw new \Exception('Не удалось создать сессию');

			self::_pdo()->commit();

			return $Session;
		} catch (\Throwable $E) {
			self::_pdo()->rollBack();
			throw $E;
		}
	}

	/**
	 * @throws \PDOException
	 * @return Session[]
	 */
	static function getByLastTime (int $sec) : array {
		$time = time() - $sec;
		$sql  = 'SELECT * FROM chat_sessions WHERE last_time>? ORDER BY last_time';
		$STM  = self::_pdo()->prepare($sql);
		$STM->execute([$time]);
		return $STM->fetchAll(\PDO::FETCH_CLASS, self::class);
	}
}