<?
declare(strict_types=1);

namespace app\models;

class User extends Base {

	const USER_ID_SYSTEM_CHAT = 5936;
	const USER_ID_THE_MAFIA   = 4321;


	/** @var int */
	public $user_id;

	/** @var string */
	public $username;

	/** @var string */
	public $username_clean;

	/** @var string */
	public $user_password;

	/** @var int */
	public $user_inactive_time;

	/**
	 * @throws \PDOException
	 * @return User|null
	 */
	static function getByLogin (string $login) {
		$sql = 'SELECT * FROM phpbb_users WHERE username_clean=?';
		$STM = self::_pdo()->prepare($sql);
		$STM->execute([strtolower($login)]);
		return $STM->fetchObject(self::class) ?: NULL;
	}

	/**
	 * @throws \PDOException
	 * @return User|null
	 */
	static function getById (int $id) {
		$sql = 'SELECT * FROM phpbb_users WHERE user_id=?';
		$STM = self::_pdo()->prepare($sql);
		$STM->execute([$id]);
		return $STM->fetchObject(self::class) ?: NULL;
	}

	/**
	 * @param int[] $ids
	 * @throws \PDOException
	 * @return User[]
	 */
	static function getByIds ($ids) {
		if (count($ids) === 0)
			return [];
		$sql = 'SELECT * FROM phpbb_users WHERE user_id IN (' . implode(',', array_map('intval', $ids)) . ')';
		return self::_pdo()->query($sql)->fetchAll(\PDO::FETCH_CLASS, self::class);
	}
}