<?
declare(strict_types=1);

namespace app\models;

class Message extends Base {

	/** @var int */
	public $id;

	/** @var int */
	public $time;

	/** @var int */
	public $user_id;

	/** @var string */
	public $text;

	/** @var string через запятую список id получателей, пустая строка означает - всем */
	public $receivers_ids;

	/** @return int[] */
	function getReceiversIdsArray () {
		if (!$this->receivers_ids)
			return [];
		return array_map('intval', explode(',', $this->receivers_ids));
	}

	/** @throws \PDOException */
	static function add (User $User, string $text, array $receiversIds = []) {
		$sql = 'INSERT INTO chat_messages (`time`,user_id,text,receivers_ids) VALUES (?,?,?,?)';
		self::_pdo()->prepare($sql)->execute([time(), $User->user_id, $text, implode(',', $receiversIds)]);
	}

	/**
	 * @throws \PDOException
	 * @return Message[]
	 */
	static function getLast (int $limit) {
		$sql = 'SELECT * FROM chat_messages ORDER BY id DESC LIMIT ' . $limit;
		return self::_pdo()->query($sql)->fetchAll(\PDO::FETCH_CLASS, self::class);
	}

	/**
	 * @throws \PDOException
	 * @return Message[]
	 */
	static function getAfterId (int $id, int $limit) {
		$sql = 'SELECT * FROM chat_messages WHERE id>? ORDER BY id DESC LIMIT ' . $limit;
		$STM = self::_pdo()->prepare($sql);
		$STM->execute([$id]);
		return $STM->fetchAll(\PDO::FETCH_CLASS, self::class);
	}

	/** @throws \PDOException */
	static function getLastId () : int {
		$sql = "SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='chat_messages'";
		return (int)self::_pdo()->query($sql)->fetchColumn() - 1;
	}
}