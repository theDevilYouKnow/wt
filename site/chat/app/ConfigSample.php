<?
declare(strict_types=1);

namespace app;

class ConfigSample {

	const TG_TOKEN_LOGS = '';
	const TG_CHAT_ADMIN = 6196104;

	const MYSQL_DB   = '';
	const MYSQL_HOST = '';
	const MYSQL_USER = '';
	const MYSQL_PASS = '';
}