<?
declare(strict_types = 1);

define('BASE_DIR', __DIR__);
header('Content-Type: text/html; charset=UTF-8');
header('Access-Control-Allow-Origin: *');

spl_autoload_register(function (string $className) {
	include __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
});

require_once __DIR__ . '/vendor/autoload.php';

set_error_handler(function (int $level, string $message, string $file, int $line, array $context) {
	if ($level == E_NOTICE)
		return FALSE;

	app\controllers\Base::_report("Warning\n\n$message\n\n$file:$line\n\n" . json_encode($context));
	return TRUE;
}, E_ALL);