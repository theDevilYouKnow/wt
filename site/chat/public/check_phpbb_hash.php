<?
declare(strict_types=1);

/**
 * Данный скрипт нельзя запускать в контексте моего приложения,
 * т.к. мой автозагрузчик будет мешать phpbb файлам.
 */

$password = (string)$_POST['password'];
$hash     = (string)$_POST['hash'];
define('IN_PHPBB', TRUE);
$phpbb_root_path = __DIR__ . '/../../forum/';
$phpEx           = 'php';
include_once(__DIR__ . '/../../forum/common.php');
echo (int)phpbb_check_hash($password, $hash);