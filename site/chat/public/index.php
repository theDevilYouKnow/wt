<?
declare(strict_types = 1);

try {
	require __DIR__ . '/../init.php';
	app\Router::start();
} catch (\Throwable $E) {
	app\Controllers\Base::_report($E->__toString());
	zPHP\API\Rend::error('Ошибка сервера', app\Controllers\Base::ERROR_CODE_SERVER);
}