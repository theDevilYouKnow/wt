"use strict";

var Chat = {

    start: function () {
        var _this = this;

        function sync() {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: 'api/getData',
                data: {last_message_id: _this.lastMessageId},
                success: function (j) {
                    if (j.status === 'error') {
                        // Игнорирование серверных ошибок (при обновлнии и тп)
                        if (j.code === 'runtime_error')
                            alert(j.message);
                        return;
                    }

                    _this.upUI(j.data);
                }
            });
        }

        sync();
        setInterval(sync, 2000);
    },

    addMessage: function ($button, $textInput) {
        var _this = this;

        $button.button('loading');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'api/addMessage',
            data: {
                text: $textInput.val(),
                receivers_ids: this.getSelectedUsersIds().join(','),
                last_message_id: _this.lastMessageId
            },
            success: function (j) {
                $button.button('reset');
                if (j.status === 'error')
                    return alert(j.message);

                $textInput.val('');
                _this.upUI(j.data);
            },
            error: function () {
                $button.button('reset');
            }
        });
    },

    generateCoin: function ($button) {
        var _this = this;

        $button.button('loading');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'api/generateCoin',
            data: {last_message_id: _this.lastMessageId},
            success: function (j) {
                $button.button('reset');
                if (j.status === 'error')
                    return alert(j.message);

                _this.upUI(j.data);
            },
            error: function () {
                $button.button('reset');
            }
        });
    },

    generateTowns: function ($button) {
        var _this = this;

        $button.button('loading');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'api/generateTowns',
            data: {last_message_id: _this.lastMessageId},
            success: function (j) {
                $button.button('reset');
                if (j.status === 'error')
                    return alert(j.message);

                _this.upUI(j.data);
            },
            error: function () {
                $button.button('reset');
            }
        });
    },

    generateTemplate: function ($button) {
        var _this = this;

        $button.button('loading');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'api/generateTemplate',
            data: {last_message_id: _this.lastMessageId},
            success: function (j) {
                $button.button('reset');
                if (j.status === 'error')
                    return alert(j.message);

                _this.upUI(j.data);
            },
            error: function () {
                $button.button('reset');
            }
        });
    },

    upUI: function (data) {
        this.upUIMessages(data.messages);
        this.upUIUsers(data.users);

        var curUser = data.cur_user;
        $('#navbar-login').text(curUser ? curUser.login : 'Гость');

        if (data.play_sound) {
            if (!_isWindowActive) {
                playSoundNewMessage();
                $('#last_play_sound_time').html(Math.floor(new Date().getTime() / 1000))
            }
        }
    },

    setIgnoreUser: function (userId, isIgnore) {
        var _this = this;

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'api/setIgnoreUser',
            data: {
                ignore_user_id: userId,
                is_ignore: isIgnore ? 1 : 0,
                last_message_id: _this.lastMessageId
            },
            success: function (j) {
                if (j.status === 'error')
                    return alert(j.message);

                _this.upUI(j.data);
            }
        });
    },


    //////////////////////
    // Рендеренг сообщений

    messagesContainerId: 'messages',
    lastMessageId: 0,

    upUIMessages: function (messages) {
        var _this = this;

        $.each(messages, function (index, message) {
            if (message.id <= _this.lastMessageId)
                return;

            _this.rendMessage(message.time, message.text);
            _this.lastMessageId = message.id;
        });
    },

    rendMessage: function (time, text) {
        function addZero(n) {
            return n < 10 ? '0' + n : n;
        }

        var messagesDiv = document.getElementById(this.messagesContainerId);
        // с небольшим запасом c учетом padding-а и в случае есть пользователь немного недокрутит
        var onTheBottom = (messagesDiv.scrollTop + $(messagesDiv).height() + 20) > messagesDiv.scrollHeight;

        var $messages = $('#' + this.messagesContainerId);

        // Время
        var myDate = new Date(time * 1000);
        var timeStr = addZero(myDate.getHours()) + ':' + addZero(myDate.getMinutes()) + ':' + addZero(myDate.getSeconds());
        var timeDate = myDate.getFullYear() + '-' + addZero(myDate.getMonth() + 1) + '-' + addZero(myDate.getDate()); // дата при наведении
        var htmlTime = '<b style="font-size:11px;" title="' + timeDate + '">[' + timeStr + ']</b>';

        var $html = $('<p style="margin-bottom:4px;">');
        $html.append(htmlTime);
        $html.append(' ' + text);
        $messages.append($html);

        if (onTheBottom)
            messagesDiv.scrollTop = messagesDiv.scrollHeight;
    },

    cleanChat: function () {
        $('#' + this.messagesContainerId).empty();
    },


    //////////////////////////
    // Рендеренг пользователей

    isOnUsersCheckboxMousedown: false, // не обновлять список пользователей при нажатой мыши
    selectedUsersIdsCache: {}, // id пользователей в виде ключей, используется только для сохранения состояния при обновлении
    selectedUsersCheckboxClass: 'chat__users-list__checkbox',
    lastUsers: null, // массив пользователей бывших онлайн в последнюю снхронизацию, null - значит первый раз

    getSelectedUsersIds: function () {
        var arr = [];
        $('.' + this.selectedUsersCheckboxClass).each(function (i, checkbox) {
            var $checkbox = $(checkbox);
            if ($checkbox.prop('checked'))
                arr.push($checkbox.data('user_id'));
        });
        return arr;
    },

    selectAllUsers: function ($checkbox) {
        // Нужно эмитировать именно клик, чтобы сработали обработчики
        $('.' + this.selectedUsersCheckboxClass).each(function (i, listCheckbox) {
            var $listCheckbox = $(listCheckbox);
            if ($listCheckbox.prop('checked') !== $checkbox.prop('checked'))
                $listCheckbox.click();
        });
    },

    // Сравнение с прошлым списком для показа кто вошел/вышел
    upJoinAndLeftUsers: function (users) {
        var _this = this;

        var usersMap = {};

        if (this.lastUsers !== null) {
            var lastUsersIds = []; // id последних пользователей
            $.each(this.lastUsers, function (index, lastUser) {
                lastUsersIds.push(lastUser.id);
                usersMap[lastUser.id] = lastUser;
            });
            var curUsersIds = []; // id текущих пользователей
            $.each(users, function (index, user) {
                curUsersIds.push(user.id);
                usersMap[user.id] = user;
            });
            var leaversIds = $(lastUsersIds).not(curUsersIds).get(); // id ливеров
            var joinedIds = $(curUsersIds).not(lastUsersIds).get(); // id подключившихся

            if (leaversIds.length > 0) {
                $.each(leaversIds, function (index, leaverId) {
                    var msg = '<b>' + usersMap[leaverId].login + '</b> has left (' + users.length + ' players)';
                    _this.rendMessage(Math.floor(new Date().getTime() / 1000), msg);
                })
            }

            if (joinedIds.length > 0) {
                $.each(joinedIds, function (index, joinedId) {
                    var msg = '<b>' + usersMap[joinedId].login + '</b> joined (' + users.length + ' players)';
                    _this.rendMessage(Math.floor(new Date().getTime() / 1000), msg);
                })
            }
        }

        this.lastUsers = users;
    },

    upUIUsers: function (users) {
        var _this = this;

        this.upJoinAndLeftUsers(users);

        if (!_this.isOnUsersCheckboxMousedown) {
            $('#num_users').text(users.length);

            var $users = $('#users').empty();
            // TODO очищать selectedUsersIdsCache от не онлайн пользователей
            $.each(users,function (index, user) {
                var $html = $('<div style="display:flex; flex-direction:row; align-items:center; padding:5px 0;">');

                var $htmlCheckbox = $('<input type="checkbox" style="margin:0 0 0 5px;" class="' + _this.selectedUsersCheckboxClass + '">')
                    .mousedown(function () {
                        _this.isOnUsersCheckboxMousedown = true;
                    })
                    .change(function () {
                        _this.isOnUsersCheckboxMousedown = false;
                        _this.selectedUsersIdsCache[user.id] = $(this).prop('checked');
                    })
                    .data('user_id', user.id)
                    .prop('checked', _this.selectedUsersIdsCache[user.id]);

                var $htmlIgnoreCheckbox = $('<input type="checkbox" style="margin:0 5px 0 5px;">')
                    .mousedown(function () {
                        _this.isOnUsersCheckboxMousedown = true;
                    })
                    .change(function () {
                        _this.isOnUsersCheckboxMousedown = false;
                        _this.setIgnoreUser(user.id, $(this).prop('checked'));
                    })
                    .prop('checked', user.is_ignored);

                $html.append($htmlCheckbox);
                $html.append($htmlIgnoreCheckbox);
                $html.append(' ' + '<span>' + user.login + '</span>');

                $users.append($html);
            });
        }
    }
};