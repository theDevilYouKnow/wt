unit WTReg;

interface

procedure Init;
function GetUserName: string;
function ReadVal(par: string): string;
function ReadValInt(par: string): integer;
procedure WriteVal(par, val: string); overload;
procedure WriteVal(par: string; val: integer); overload;

implementation

uses
  Registry, SysUtils, Windows, StrUtils;

var
  RegSubKey: string;

procedure WriteIfNotExists(par: string; val: integer); overload;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if (Reg.OpenKey(RegSubKey, True)) and not Reg.ValueExists(par) then
    begin
      WriteVal(par, val);
    end;
  finally
    Reg.Free;
  end;
end;

procedure WriteIfNotExists(par, val: string); overload;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if (Reg.OpenKey(RegSubKey, True)) and not Reg.ValueExists(par) then
    begin
      WriteVal(par, val);
    end;
  finally
    Reg.Free;
  end;
end;

procedure Init;
var
  pcRegSubKey: pchar;
  WTRegKey: string;
  Reg: TRegistry;
begin
  WTRegKey := 'SOFTWARE\Heroes 3 World Tournament\';
  pcRegSubKey := pchar(pinteger($63FF84)^);
  RegSubKey := StrPas(pcRegSubKey);
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  CopyMemory(Ptr(PInteger($63FF84)^), @WTRegKey[1], Length(WTRegKey) + 1);
  RegSubKey := WTRegKey;
  WriteVal('AppPath', AnsiReplaceText(ExtractFileDir(GetCommandLine), '"', ''));
  WriteIfNotExists('Show Intro', 0);
  WriteIfNotExists('Music Volume', 0);
  WriteIfNotExists('Sound Volume', 0);
  WriteIfNotExists('Last Music Volume', 0);
  WriteIfNotExists('Last Sound Volume', 0);
  WriteIfNotExists('Walk Speed', 2);
  WriteIfNotExists('Computer Walk Speed', 2);
  WriteIfNotExists('Show Route', 1);
  WriteIfNotExists('Move Reminder', 1);
  WriteIfNotExists('Quick Combat', 1);
  WriteIfNotExists('Video Subtitles', 0);
  WriteIfNotExists('Town Outlines', 1);
  WriteIfNotExists('Animate SpellBook', 0);
  WriteIfNotExists('Window Scroll Speed', 1);
  WriteIfNotExists('Bink Video', 0);
  WriteIfNotExists('Blackout Computer', 0);
  WriteIfNotExists('First Time', 0);
  WriteIfNotExists('Test Decomp', 1);
  WriteIfNotExists('Test Read', 1);
  WriteIfNotExists('Test Blit', 1);
  WriteIfNotExists('Unique System ID', 'GEX');
  WriteIfNotExists('Network Default Name', 'Player');
  WriteIfNotExists('Autosave', 1);
  WriteIfNotExists('Show Combat Grid', 1);
  WriteIfNotExists('Show Combat Mouse Hex', 1);
  WriteIfNotExists('Combat Shade Level', 1);
  WriteIfNotExists('Combat Army Info Level', 1);
  WriteIfNotExists('Combat Auto Creatures', 1);
  WriteIfNotExists('Combat Auto Spells', 0);
  WriteIfNotExists('Combat Catapult', 1);
  WriteIfNotExists('Combat Ballista', 1);
  WriteIfNotExists('Combat First Aid Tent', 1);
  WriteIfNotExists('Combat Speed', 2);
  WriteIfNotExists('Main Game Show Menu', 0);
  WriteIfNotExists('Main Game X', 0);
  WriteIfNotExists('Main Game Y', 0);
  WriteIfNotExists('Main Game Full Screen', 1);
  WriteIfNotExists('WoG_Version', 'World Tournament 0.2, 24.06.2009');
  WriteIfNotExists('Simultaneous Turn On', 0);
  WriteIfNotExists('Simultaneous Turn Date', 116);
  WriteIfNotExists('Chess Time Start', 4000);
  WriteIfNotExists('Chess Time Add', 500);
end;

function GetUserName: string;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_LOCAL_MACHINE;
  Reg.OpenKey(RegSubKey, False);
  Result := Reg.ReadString('Network Default Name');
  Reg.Free;
end;

function ReadVal(par: string): string;
var
  Reg: TRegistry;
begin
  Result := '';
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    Reg.OpenKey(RegSubKey, False);
    Result := Reg.ReadString(par);
  finally
    Reg.Free;
  end;
end;

function ReadValInt(par: string): integer;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    Reg.OpenKey(RegSubKey, False);
    Result := Reg.ReadInteger(par);
  finally
    Reg.Free;
  end;
end;

procedure WriteVal(par, val: string);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(RegSubKey, True) then
    begin
      Reg.WriteString(par, val);
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure WriteVal(par: string; val: integer);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(RegSubKey, True) then
    begin
      Reg.WriteInteger(par, val);
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

end.

