unit MainForm;

interface

uses
    Forms, Classes, Controls, OleCtrls, SHDocVw, Windows, StdCtrls, Dialogs,
    Messages, activex, MSHTML, SysUtils, ExtCtrls, ShellApi, Variants,
    SettingsForm;

type
  TFrmMain = class(TForm)
    WebBrowserChat: TWebBrowser;
    ButtonStartGame: TButton;
    btnSettings: TButton;
    timerCheckBrowserLastPlaySoundTime: TTimer;
    procedure WebBrowserChatBeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
    procedure WebBrowserChatDocumentComplete(ASender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure timerCheckBrowserLastPlaySoundTimeTimer(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ButtonStartGameClick(Sender: TObject);
  private
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
    procedure MyMessageHandler(var Msg: TMsg; var Handled: Boolean); // ��� ������ ������ Enter ������ ���� ����
  public
  end;

var
  frmMain: TFrmMain;
  browserLastPlaySoundTime: string;
  SaveMessageHandler: TMessageEvent; // ��� ������ ������ Enter ������ ���� ����
  chatUrl:string = 'http://heroeswt.net/chat/public?delphi_client_version=61';


implementation

{$R *.dfm}

function BrowserGetElementById(const Doc: IDispatch; const Id: string): IDispatch;
var
  Document: IHTMLDocument2;     // IHTMLDocument2 interface of Doc
  Body: IHTMLElement2;          // document body element
  Tags: IHTMLElementCollection; // all tags in document body
  Tag: IHTMLElement;            // a tag in document body
  I: Integer;                   // loops thru tags in document body
begin
  Result := nil;
  // ��������� ������������ ���������: ��������� ��������� IHTMLDocument2
  if not Supports(Doc, IHTMLDocument2, Document) then
    raise Exception.Create('Invalid HTML document');
  // Check for valid body element: require IHTMLElement2 interface to it
  if not Supports(Document.body, IHTMLElement2, Body) then
    raise Exception.Create('�� ���� ����� ������� ');
  // �������� ��� ���� ��������� � body ('*' => ����� ��� ����)
  Tags := Body.getElementsByTagName('*');
  // ��������� ��� ���� � body
  for I := 0 to Pred(Tags.length) do
  begin
    // �������� ������ �� ����
    Tag := Tags.item(I, EmptyParam) as IHTMLElement;
    // Check tag's id and return it if id matches
    if AnsiSameText(Tag.id, Id) then
    begin
      Result := Tag;
      Break;
    end;
  end;
end;

procedure TFrmMain.btnSettingsClick(Sender: TObject);
begin
  frmSettings.ShowModal;
end;

procedure TFrmMain.ButtonStartGameClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFrmMain.FormActivate(Sender: TObject);
begin
  SaveMessageHandler := Application.OnMessage; // ��� ������ ������ Enter ������ ���� ����
  Application.OnMessage := MyMessageHandler; // ��� ������ ������ Enter ������ ���� ����
end;

procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmMain.WebBrowserChat.Free;
  frmSettings.Free;
  ExitProcess(0);
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  frmSettings := TfrmSettings.Create(nil);
  WebBrowserChat.Navigate(chatUrl);
end;

procedure TFrmMain.FormDeactivate(Sender: TObject);
begin
  Application.OnMessage := SaveMessageHandler; // ��� ������ ������ Enter ������ ���� ����
end;

{ ��� ������ ������ Enter ������ ���� ���� }
procedure TFrmMain.MyMessageHandler(var Msg: TMsg; var Handled: Boolean);
begin
  if (Msg.message = WM_KEYDOWN) and (Msg.wParam = VK_RETURN) then
    TranslateMessage(Msg);
end;

{ �� ���������� �������� ��� ���� ������� �� �������� ������������ ������� ����� }
procedure TFrmMain.WMSysCommand(var Msg: TWMSysCommand);
begin
  if Msg.CmdType = SC_MINIMIZE then
    ShowWindow(Handle, SW_MINIMIZE)
  else
    inherited;
end;

{ �������� �� ��������� ����������� �����, � ����������� ��� ������������� }
procedure TFrmMain.timerCheckBrowserLastPlaySoundTimeTimer(Sender: TObject);
var
  Elem: IHTMLElement;
begin
  Elem := BrowserGetElementById(frmMain.WebBrowserChat.Document, 'last_play_sound_time') as IHTMLElement;
  if Assigned(Elem) then
  begin
    if browserLastPlaySoundTime <> Elem.innerHTML then
      FlashWindow(handle, True);
    browserLastPlaySoundTime := Elem.innerHTML;
  end;
end;

{ ��� �������� ������ � �������� �� ��������� }
procedure TFrmMain.WebBrowserChatBeforeNavigate2(ASender: TObject; const pDisp: IDispatch;
  var URL, Flags, TargetFrameName, PostData, Headers: OleVariant;
  var Cancel: WordBool);
begin
  if chatUrl = PChar(VarToStr(URL)) then
    Exit;
  ShellExecute(Handle, 'Open', PChar(VarToStr(URL)), nil, nil, SW_SHOWNORMAL);
  Cancel := True;
end;

{ ����� ������� ������������ ����� ��������� ������ ����� �������� �������� }
procedure TFrmMain.WebBrowserChatDocumentComplete(ASender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
  timerCheckBrowserLastPlaySoundTime.Enabled := true;
  WebBrowserChat.SetFocus;
end;

initialization
  OleInitialize(nil);

finalization
  OleUninitialize;

end.
