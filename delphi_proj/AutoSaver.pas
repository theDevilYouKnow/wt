unit AutoSaver;

interface

procedure Init;


implementation

uses
  SysUtils, Windows,
  MyUtils;

var
  AutoSaveName: string;

procedure RenameAutoSave;
var
  f2: string;
  day: word;
  week: word;
  month: word;
  P: POINTER;
  Len: Integer;
  plName: array[0..1] of string;
  i: integer;
begin
  day:=PWORD(PINTEGER($699538)^+$1F63E)^;
  week:=PWORD(PINTEGER($699538)^+$1F640)^;
  month:=PWORD(PINTEGER($699538)^+$1F642)^;
  for i:=0 to 1 do
  begin
    P:=POINTER(PINTEGER($699538)^+$20AD0+i*$168+$CC); // ��� ������
    Len:=SysUtils.StrLen(PCHAR(P));
    SetLength(plName[i], Len);
    CopyMemory(@plName[i][1], PCHAR(P), Len);
  end;
  f2:='.\games\'+ plName[0] + ' vs ' + plName[1]
  + ' [' + IntToStr(PINTEGER($69CCF4)^+1) + '] '
  + IntToStr(month) + IntToStr(week) + IntToStr(day)
  + Copy(AutoSaveName, Pos('.G', AutoSaveName)); // ����������
  SysUtils.DeleteFile(f2);
  SysUtils.RenameFile(AutoSaveName, f2);
end;

procedure HookAutoSave;
asm
  // ��������� ������ ���
  MOV EAX, $4BEB60
  call eax
  //
  PUSHAD
  CALL RenameAutoSave
  POPAD
  //
  PUSH $4C6DD3
end;

procedure GetAutoSaveName;
var
  Len: Integer;
  P: POINTER;
begin
  asm
    MOV P, EAX
  end;
  Len:=SysUtils.StrLen(PCHAR(P));
  SetLength(AutoSaveName, Len);
  CopyMemory(@AutoSaveName[1], PCHAR(P), Len);
end;

procedure HookGetAutoSaveName;
asm
  PUSHAD
  LEA EAX, [ebp-$454] // ����� ������ ��� ���������
  CALL GetAutoSaveName
  POPAD
  // ������ ���
  MOV EAX, $677D80
  //
  PUSH $4BED6D
end;

procedure Init;
begin
    HookCode(POINTER($4BED68), @HookGetAutoSaveName, C_HOOKTYPE_JUMP);
    HookCode(POINTER($4C6DCE), @HookAutoSave, C_HOOKTYPE_JUMP);
end;

end.

