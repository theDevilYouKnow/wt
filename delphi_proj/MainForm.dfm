object FrmMain: TFrmMain
  Left = 439
  Top = 110
  Caption = 'Heroes 3: World Tournament'
  ClientHeight = 561
  ClientWidth = 855
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 740
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  DesignSize = (
    855
    561)
  PixelsPerInch = 96
  TextHeight = 13
  object WebBrowserChat: TWebBrowser
    Left = 0
    Top = 0
    Width = 855
    Height = 561
    Align = alClient
    TabOrder = 0
    OnBeforeNavigate2 = WebBrowserChatBeforeNavigate2
    OnDocumentComplete = WebBrowserChatDocumentComplete
    ExplicitWidth = 685
    ExplicitHeight = 345
    ControlData = {
      4C0000005E580000FB3900000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object ButtonStartGame: TButton
    Left = 762
    Top = 528
    Width = 85
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = #1053#1072#1095#1072#1090#1100' '#1080#1075#1088#1091
    TabOrder = 1
    OnClick = ButtonStartGameClick
  end
  object btnSettings: TButton
    Left = 681
    Top = 528
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
    TabOrder = 2
    OnClick = btnSettingsClick
  end
  object timerCheckBrowserLastPlaySoundTime: TTimer
    Enabled = False
    Interval = 250
    OnTimer = timerCheckBrowserLastPlaySoundTimeTimer
    Left = 600
    Top = 512
  end
end
