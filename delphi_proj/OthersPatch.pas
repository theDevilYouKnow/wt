unit OthersPatch;

interface

uses
  SysUtils, Windows,
  MyUtils, MainForm;

procedure Init;

implementation

procedure HookWogMsgStartGame;
asm
  mov [$7B6648], eax // ��������� ������ ���
  push $7055BF // ������� ����� ����� ������ ��������� ��� ��� ��� �� �����
end;

// ������ ��� �� � Wog options ���� ����� ������ �� �������
procedure HookSetRadioButton;
asm
  // ��������� ������ ���
  mov eax, $291A454
  mov [eax], 2
  // ��� ���
  mov eax, $291A8B0
  mov [eax], 2
  // ������������ �����
  PUSH $7786C4
end;

// ��������� ����������� �������� ��� ��������� ����
// ��� ��������� ��������� ���� �� ���� ????
// TODO: ���������� ������ ����� � ���-�� ������� � ����������� �� ������� � rmg.txt
procedure HookSetStandartRMapConfig;
asm
  // ��������� ������ ���
  mov [ebx+$18BC], eax
  // ��� ���
  // ������ �����
  mov dword ptr [ebx+$18A0], 108
  //������ 1-���, 2-����
  //mov [ebx+$18A4], 2
  mov dword ptr [ebx+$18A8], 2
  mov dword ptr [ebx+$18AC], 0
  mov dword ptr [ebx+$18B0], 0
  mov dword ptr [ebx+$18B4], 0
  // ����
  mov dword ptr [ebx+$18B8], 0
  // ���� ��������
  mov dword ptr [ebx+$18BC], 2
  // ������ ����������� (P5) - ������������ �� ��� �������� (2)
  mov eax, $027728D4
  mov [eax], 2
  // ������������ �����
  PUSH $57D4A6
end;

procedure HookChangeHeaderSave;
asm
  // ��������� ������ ���
  mov eax, $4BC410
  call eax
  mov [ebp-$5CC], $2A
  PUSH $4BCB16
end;

procedure DeleteRandomMap;
var
  filename: string;
  Len, AntiCheatOn: Integer;
  P: POINTER;
begin
  // �������� ��� ����� ������� �������� $277273C = P903
  AntiCheatOn := PINTEGER($277273C)^;
  if AntiCheatOn = 0 then Exit;
  // ������� �����
  P := POINTER($699430); // $699430 ��� �����
  Len:=SysUtils.StrLen(P);
  SetLength(filename, Len);
  CopyMemory(@filename[1], P, Len);
  SysUtils.DeleteFile('.\random_maps\' + filename);
  //MessageBox(0, PCHAR('.\random_maps\' + filename), 'asdfasdf', MB_OK)
end;

// ����� ����� ��� ��������� ����� � �������
procedure HookDeleteRandomMap;
asm
  // ��������� ������ ���
  MOV eax, $4C01E0
  call eax
  // ��� ���
  PUSHAD
  CALL DeleteRandomMap
  POPAD
  // ����������
  PUSH $58C9A3
end;

// ����� ������� ������
procedure HookRefugee;
asm
  cmp al, 0
  je @@Exit
  mov [edi+22h], dx
  @@Exit:
  test al, al
  // ����������
  PUSH $505E1B
end;

procedure Init;
var
  Temp: integer; // ��������� ���������� ��� ��������
begin
  // WoG 3.58 was successfully tested in a network ....
  HookCode(Ptr($7055A3), @HookWogMsgStartGame, MyUtils.C_HOOKTYPE_JUMP);
  // ����� ������ ��� �����
  HookCode(POINTER($7786BA), @HookSetRadioButton, C_HOOKTYPE_JUMP);
  HookCode(POINTER($57D4A0), @HookSetStandartRMapConfig, C_HOOKTYPE_JUMP);
  // ������ � ��������� ������ � 29 �� 2A
  HookCode(POINTER($4BCB11), @HookChangeHeaderSave, C_HOOKTYPE_JUMP);
  // ������ ����� ����. H (H3SVG) � ����� �� ������� �
  Temp:=$29;
  WriteAtCode(Ptr($4BBFE0), @Temp, 1);
  WriteAtCode(Ptr($4BC066), @Temp, 1); // ����
  WriteAtCode(Ptr($4BCAFC), @Temp, 1); // ��������
  WriteAtCode(Ptr($4BE14B), @Temp, 1);
  // �������� ������ �����
  HookCode(POINTER($58C99E), @HookDeleteRandomMap, C_HOOKTYPE_JUMP);
  // ����������� ������ ��������
  HookCode(POINTER($505E15), @HookRefugee, C_HOOKTYPE_JUMP, 6);
  // ����-���� �����: 200% - 2500 �����
  Temp:=2500;
  WriteAtCode(Ptr($6781F8), @Temp, 4);
end;

end.

