library asmwt;

uses
  Forms, Windows,
  MainForm in 'MainForm.pas' {frmMain},
  MyUtils in 'MyUtils.pas',
  SettingsForm in 'SettingsForm.pas' {frmSettings},
  WTReg in 'WTReg.pas',
  Era in 'Era.pas',
  QuickCombatPatch in 'QuickCombatPatch.pas',
  OthersPatch in 'OthersPatch.pas',
  AutoSaver in 'AutoSaver.pas';

var
  hEra: Windows.THandle; { ���������� ���������� }

procedure StartMainForm;
begin
  Application.Initialize;
  Application.CreateForm(TFrmMain, frmMain);
  Application.Run;
end;

procedure HookGameExeStart;
asm
  pusha
  call StartMainForm
  popa
  mov eax, $4F80C0
  call eax
  push $61A964
end;

begin
  { ����������, ��� DllMain �� ����� ������� 1000 � 1 ��� ��-�� ������ }  
  hEra:=Windows.GetModuleHandle('asmwt.dll');
  Windows.DisableThreadLibraryCalls(hEra);

  { ����� ���� ���������: ���������� ��++ ��� �����������, ���� ��� �� �������� }
  MyUtils.HookCode(Ptr($61A95F), @HookGameExeStart, C_HOOKTYPE_JUMP); { ������ ������� ����� }
  WTReg.Init;
  Era.Init; 
  OthersPatch.Init;
  QuickCombatPatch.Init;
  AutoSaver.Init;
end.
