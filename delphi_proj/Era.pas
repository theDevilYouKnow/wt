unit Era;

interface

uses Windows, MyUtils;

const
  (* �����: ������ *)
  C_PCHAR_SAVEGAME_NAME = $69FC88; // ��� ����������� ���� (�����)
  C_PCHAR_LASTSAVEGAME_NAME = $68338C; // ��� ���������� ������������/������������ �����
  (* �����: ������� *)
  C_FUNC_PLAYSOUND = $59A890; // ������� PlaySound
  C_FUNC_ZVS_TRIGGERS_PLAYSOUND = $774F0A; // ����������� PlaySound �� ZVS
  C_FUNC_ZVS_ERMMESSAGE = $70FB63; // ������� IF:M
  C_FUNC_ZVS_PROCESSCMD = $741DF0; // ProcessCmd
  C_FUNC_SAVEGAME_HANDLER = $4180C0; // ����������: ����-> ���������
  C_FUNC_SAVEGAME = $4BEB60; // ������� ���������� ����
  C_FUNC_ZVS_GZIPWRITE = $704062; // ������� GZipWrite(Address: POINTER; Count: INTEGER); CDECL;
  C_FUNC_ZVS_GZIPREAD = $7040A7; // ������� GZipRead(Address: POINTER; Count: INTEGER); CDECL;
  C_FUNC_ZVS_GETHEROPTR = $71168D; // ������� GetHeroPtr(HeroNum: INTEGER); CDECL;
  C_FUNC_ZVS_CALLFU = $74CE30; // void FUCall(int n)
  (* �����: ���������� *)
  C_VAR_ERM_V = $887668; // ���-���������� v1
  C_VAR_ERA_COMMAND = C_VAR_ERM_V + 4*9949; // ������� ��� ��� ��� ���������� v9950
  C_VAR_ERA_APIRESULT = C_VAR_ERM_V + 4*9998; // ��������� ���������� ������� API ����� ������ ���
  C_VAR_ERM_Z = $9273E8; // ���-���������� z1
  C_VAR_ERM_Y = $A48D80; // ERM-���������� y1
  C_VAR_ERM_X = $91DA38; // ERM-���������� x1
  C_VAR_ERM_HEAP = $27F9548; // ��������� �� ������ ������� (_Cmd_)
  C_VAR_ERM_PTR_CURRHERO = $27F9970; // ������� �����
  C_VAR_HWND = $699650; // ���������� ���� ������

  C_ERA_EVENT_BATTLE_WHOMOVES = 77006; //  �������: ��� ������ ����� � �����
  C_ERA_EVENT_BATTLE_BEFOREACTION = 77007; //  �������: �� �������� (�� ���� ���)

procedure Init;


implementation

procedure GenerateCustomErmEvent(ID: INTEGER); assembler;
asm
  PUSH ID
  MOV EAX, C_FUNC_ZVS_CALLFU
  CALL EAX
  ADD ESP, 4
end;

procedure HookBattleWhoMoves; assembler;
asm
  // ������������� ���������� ����
  PUSH EBP
  MOV EBP,ESP
  PUSH -1
  // ���������� ��� �������
  PUSHAD
  MOV EAX, [EBP+$8]
  MOV DWORD [C_VAR_ERM_X], EAX
  MOV EAX, [EBP+$C]
  MOV DWORD [C_VAR_ERM_X+4], EAX
  MOV EAX, C_ERA_EVENT_BATTLE_WHOMOVES
  CALL GenerateCustomErmEvent
  POPAD
  // ������������� ����������� �� ��� ��������
  MOV EAX, DWORD [C_VAR_ERM_X]
  MOV [EBP+$8], EAX
  MOV EAX, DWORD[C_VAR_ERM_X+4]
  MOV [EBP+$C], EAX
  PUSH $464F15
end;


procedure Init;
var
  Temp: INTEGER; // ��������� ���������� ��� ��������
begin
  // ������ ������ ������� FindErm, ����� ���� ��������� ������� � ������ ��������
  Temp:=$EB;
  WriteAtCode(Ptr($74A724), @Temp, 1);
  // ������ ����� �������, ��� ������ �����
  HookCode(Ptr($464F10), @HookBattleWhoMoves, C_HOOKTYPE_JUMP, 5);
end;


end.
