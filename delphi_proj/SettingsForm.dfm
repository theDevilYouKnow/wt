object frmSettings: TfrmSettings
  Left = 0
  Top = 0
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 27
    Width = 92
    Height = 13
    Caption = #1053#1072#1095#1072#1083#1100#1085#1086#1077' '#1074#1088#1077#1084#1103':'
  end
  object Label2: TLabel
    Left = 183
    Top = 27
    Width = 75
    Height = 13
    Caption = #1077#1078#1077#1076#1085#1077#1074#1085#1086' + :'
  end
  object eChessTimeStart: TEdit
    Left = 105
    Top = 24
    Width = 48
    Height = 21
    MaxLength = 5
    TabOrder = 0
    Text = '4000'
    OnChange = eChessTimeStartChange
  end
  object eChessTimeAdd: TEdit
    Left = 264
    Top = 24
    Width = 41
    Height = 21
    MaxLength = 4
    TabOrder = 1
    Text = '600'
    OnChange = eChessTimeAddChange
  end
end
