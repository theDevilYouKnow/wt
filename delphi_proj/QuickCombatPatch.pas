unit QuickCombatPatch;

interface

uses
  SysUtils, Windows,
  MyUtils, MainForm, Era;

procedure Init;


implementation

var
  BoxPcx: string = 'Box64x30.pcx';
  CancelDef: string = 'iCancel.def';
  font: string = 'BigFont.fnt';
  //AutoCombatText: string = '��������� ���������� ���������?';
  RePlay: integer = 0;
  NewQC: integer = 0;
  NeedChangeArts: integer = 0;
  tmp: integer = 0;
  res: integer; // ����� ��� ��� ������������ �����

procedure HookDrawButtonOnCombatResultPanel;
asm
  // ������ "Ok" �� Esc
  mov eax, $4712A1
  mov [eax], 1
  // ���� ��������� ���������, �� �������
  mov eax, $6987CC
  cmp DWORD [eax], 0
  je @@Exit
  // ������� "Ok" � Esc � ������ ��� ��� �� Enter
  mov eax, $4712A1
  mov [eax], 28
  // ������ box ��� cancel
  push $34
  mov eax, $617492
  call eax // ��������� ������ ��� ����
  add esp, 4
  mov ecx, eax
  push $800
  push BoxPcx
  push $C8
  push $20
  push $42
  push $1FA
  push 20
  mov eax, $44FFA0
  call eax // ������ ����
  lea ecx, tmp
  mov tmp, eax
  mov eax, [ebx+8]
  push ecx
  push 1
  push eax
  mov ecx, ebx
  mov eax, $5FE2D0
  call eax // ��������� �� ������
  // ������ iCancel.def
  push 104
  mov eax, $617492
  call eax // ��������� ������ ��� ����
  add esp, 4
  mov ecx, eax
  push 2
  push 1 // Hotkey Ecs=1, Enter=28
  push 1 // 1 - ��� �� ����������� ����� �������
  push 1
  push 0
  push CancelDef
  push $7803
  push $1E
  push $40
  push $1FB
  push 21
  mov eax, $455BD0
  call eax
  lea ecx, tmp
  mov tmp, eax
  mov eax, [ebx+8]
  push ecx
  push 1
  push eax
  mov ecx, ebx
  mov eax, $5FE2D0
  call eax // ��������� �� ������
  // ����� �����
  push 80
  mov eax, $617492
  call eax // ��������� ������ ��� ����
  add esp, 4
  mov ecx, eax

  push 8 // int unused
  push 0 // int bkcolor
  push 1 // int align
  push $C8 // int itemId
  push 4 // color
  push font // font
  PUSH C_VAR_ERM_Z+700*512 // z701 - ��������� ��������� ���������?
  //push AutoCombatText
  push 26
  push 290
  push 510
  push 90
  mov eax, $5BC6A0
  call eax
  //sub_5BC6A0(v18, 89, 37, 115, 20, v7 + 35, (int)"smalfont.fnt", 4, 201, 0, 0, 8);

  lea ecx, tmp
  mov tmp, eax
  mov eax, [ebx+8]
  push ecx
  push 1
  push eax
  mov ecx, ebx
  mov eax, $5FE2D0
  call eax // ��������� �� ������
  @@Exit:
  // ��������� ������ ���
  push $34
  MOV eax, $617492
  call eax
  add esp, 4
  // ����������
  PUSH $470F6C
end;


procedure Ask_Replay;
begin
  {
  // 30723 - ������, 30722 - ��
  // ���� ������� ����� ���������, �� �� ������� ������
  mov eax, $6987CC
  cmp DWORD [eax], 0
  je @@Exit
  // �������� ������
    PUSH 0
    PUSH 2 // 2 - ������
    PUSH C_VAR_ERM_Z+700*512 // z701 - ��������� - �� ������ ���������� �����?
    MOV EAX, C_FUNC_ZVS_ERMMESSAGE
    CALL EAX
    ADD ESP, $0C
    cmp eax, $7805  // ��� - $7806, �� - $7805
    jne @@Exit // ���� �� ����� ��, �� �� �����
      mov RePlay, 1;
      mov eax, C_VAR_ERM_V+700*4 // v701
      mov [eax], 1 // v701 := 1
  @@Exit:
  }
  // ���� ������� ����� ���������, �� �� ������� ������
  if (PINTEGER($6987CC)^ = 0) then begin
    //if (RePlay = 0) then NewQC := 0;
    Exit;
  end;
  // ���� �� ����� ��
  // 30723 - ������, 30722 - �� (����������� � ������)
  if res = 30722 then begin
    if (NeedChangeArts = 1) then begin
      asm

      end;
    end;
    Exit;
  end;
  RePlay := 1;
  PINTEGER(C_VAR_ERM_V+700*4)^ := 1;  // v701 := 1

//  if (NeedChangeArts) then

end;


// ����� ������� Ok �� ����������� ����� ��� ��������
procedure HookCombatResultWinPanelOk;
asm
  // ��������� ������ ���
  MOV eax, $4716C0
  call eax
  // ��� ���
  mov eax, $6992D0
  mov eax, [eax]
  mov eax, [eax+$38]
  mov res, eax
  PUSHAD
  call Ask_Replay;
  POPAD
  // ��������� ������, ���� ��� ����� � ��� �����������
  cmp RePlay, 1;
  je @@Exit2
  cmp NeedChangeArts, 1;
  jne @@Exit2
    //.text:004770AB  lea     eax, [ebp+var_38]
    //.text:004770AE  mov     ecx, esi
    //.text:004770B0  push    eax
    //.text:004770B1  push    edi
    //.text:004770B2  call    sub_469C50      ; ����������� ������
    lea eax, [ebp-$38]
    mov ecx, esi
    push eax
    push edi
    mov eax, $469C50
    call eax
  @@Exit2:
  mov NeedChangeArts, 0
  // ����������
  PUSH $477245
end;

// ����� ������� Ok �� ����������� ����� ��� ���������
procedure HookCombatResultLosePanelOk;
asm
  // ��������� ������ ���
  MOV eax, $4716C0
  call eax
  // ��� ���
  mov eax, $6992D0
  mov eax, [eax]
  mov eax, [eax+$38]
  mov res, eax 
  PUSHAD
  call Ask_Replay;
  POPAD
  // ��������� ������, ���� ��� ����� � ��� �����������
  cmp RePlay, 1;
  je @@Exit2
  cmp NeedChangeArts, 1;
  jne @@Exit2
    //.text:004770AB  lea     eax, [ebp+var_38]
    //.text:004770AE  mov     ecx, esi
    //.text:004770B0  push    eax
    //.text:004770B1  push    edi
    //.text:004770B2  call    sub_469C50      ; ����������� ������
    lea eax, [ebp-$38]
    mov ecx, esi
    push eax
    push edi
    mov eax, $469C50
    call eax
  @@Exit2:
  mov NeedChangeArts, 0
  // ����������
  PUSH $4772F4
end;

// ������ ����������� �����
// ��� ��� ������ �������, ������� ������ �������� �
// � �������� ����� �������
procedure HookCombatNoCommitResult;
asm
  // ��������� ������ ���
  // ������ ������ �� ���������
  // ����������
  PUSH $476FE3
end;

// ������ ����������� �����
// ����� ��������
procedure HookCombatCommitResult;
asm
  // ��������� ������ ���
  // mov     ds:dword_6977D4, 0
  mov eax, $6977D4
  mov [eax], 0
  // ��� ���
  pushad
  cmp RePlay, 1 // ���� ������� ����� ����������, �� ���������� ���� �� ����������
  je @@Exit // �� ����� ���� �����
  push 0 // ��������� ?
  mov ecx, esi // Combat Manager
  mov eax, $463DD0
  call eax
  push 1 // ������������?
  mov ecx, esi // Combat Manager
  mov eax, $463DD0
  call eax
  @@Exit:
  popad
  // ����������
  PUSH $477310
end;

// ������ ����������� �����
// ����� ��������
procedure HookCombatBegin;
asm
  // ��������� ������ ���
  //.text:0075AEB0 mov     eax, offset sub_4AD160
  // �������� ���� ��� �������� �� ������ ���������
  mov eax, $75ABB6
  call eax; // BA1 - ��������� �����
  // ��� ���
  pushad
  cmp RePlay, 1
  jne @@NoReplay
    mov eax, $6987CC // ��������� ������� �����
    mov [eax], 0 // ������� ������� �����
    mov RePlay, 0
    // v701 ������ ������� ����� ����� !?B53 
    // mov eax, C_VAR_ERM_V+700*4 // v701
    // mov [eax], 0 // v701 := 0
    mov NewQC, 1
    popad
    PUSH $75AE1A // $75AE19 // ������������ ����� ��� ��������������� ������
    ret
  @@NoReplay:
  cmp NewQC, 0
  je @@Exit
    mov NewQC, 0
    mov eax, $6987CC // ��������� ������� �����
    mov [eax], 1 // ���� ����� ��������, �� �������� ������� ����� �����
  @@Exit:
  popad
  // ����������
  PUSH $75AEC2
end;

// ��� ��������� ��� ����� � ������ ���������
// ���� ������, �� �������
procedure HookRemoveHero;
asm
  // ��������� ������ ���
  //.text:004AE3BF push    0
  //.text:004AE3C1 push    1
  //.text:004AE3C3 mov     ecx, edi
  //.text:004AE3C5 call    sub_4DA130 ; ����� ��������� � ���� ����� � ������ ���������
  cmp RePlay, 1
  je @@Exit
    push 0
    push 1
    mov ecx, edi
    mov eax, $4DA130
    call eax
  @@Exit:
  // ����������
  PUSH $4AE3CA
end;

// ��� ��������� ����� ���������� � ������ ���������
// ���� ������, �� �������
procedure HookRemoveHero2;
asm
  // ��������� ������ ���
  cmp RePlay, 1
  je @@Exit
    push 0
    push 1
    mov ecx, ebx
    push $4AE4F3
    ret
  @@Exit:
  // ����������
  PUSH $4AE3CA
end;

// ���������� �����
// ���� ������, �� ��������� �� ����
procedure HookAddExp;
asm
  cmp RePlay, 1
  je @@Exit
    mov al, [edi+esi+$54A6]
    PUSH $47726A
    ret
  @@Exit:
  // ���������� ����� ���������
  PUSH $47727D
end;

// ������������ ������� (�� ������� � ����������� �����)
// ��� ������ ���������� � ��������� ���� ��� ���� ���������� ��������
procedure HookChangeArts;
asm
  mov NeedChangeArts, 1
  // ���������� ����� ���������
  PUSH $4770B7
end;

// ��������� ���������� ����� ������
// ���� Reaplay, �� ����������
procedure HookGarnisonArmy;
asm
  cmp Replay, 1
  je @@Skip
    mov eax, $4DA130
    call eax
    jmp @@Exit
  @@Skip:
  pop eax;
  pop eax;
  @@Exit:
  // ���������� ����� ���������
  PUSH $4AE46E
end;

procedure Init;
begin
  HookCode(POINTER($477240), @HookCombatResultWinPanelOk, C_HOOKTYPE_JUMP);
  HookCode(POINTER($4772EF), @HookCombatResultLosePanelOk, C_HOOKTYPE_JUMP);
  HookCode(POINTER($476FD2), @HookCombatNoCommitResult, C_HOOKTYPE_JUMP, 16);
  HookCode(POINTER($477306), @HookCombatCommitResult, C_HOOKTYPE_JUMP, 10);
  HookCode(POINTER($75AEBD), @HookCombatBegin, C_HOOKTYPE_JUMP);
  HookCode(POINTER($4AE3BF), @HookRemoveHero, C_HOOKTYPE_JUMP, 11);
  HookCode(POINTER($4AE4ED), @HookRemoveHero2, C_HOOKTYPE_JUMP, 6);
  HookCode(POINTER($477263), @HookAddExp, C_HOOKTYPE_JUMP, 7);
  HookCode(POINTER($4770B0), @HookChangeArts, C_HOOKTYPE_JUMP, 7);
  HookCode(POINTER($4AE469), @HookGarnisonArmy, C_HOOKTYPE_JUMP, 5);

  HookCode(POINTER($470F62), @HookDrawButtonOnCombatResultPanel, C_HOOKTYPE_JUMP, 10);
//  HookCode(POINTER($471574), @Hook_Draw_Button_On_Combat_Result_Panel, C_HOOKTYPE_JUMP, 5);

end;

end.

