unit MyUtils;

interface      

uses
  SysUtils, Windows, Classes, Forms;

const
  C_HOOKTYPE_JUMP = FALSE;
  C_HOOKTYPE_CALL = TRUE;

procedure WriteAtCode(P: POINTER; Buf: POINTER; Count: INTEGER);
procedure HookCode(P: POINTER; NewAddr: POINTER; UseCall: BOOLEAN; PatchSize: INTEGER); overload;
procedure HookCode(P: POINTER; NewAddr: POINTER; UseCall: BOOLEAN); overload;
procedure ChangeLineInFile(FileName, SearchSubLine, NewLine: string);


implementation

const
  C_OPCODE_JUMP = $E9;
  C_OPCODE_CALL = $E8;
  C_UNIHOOK_SIZE = 5;

type
  // ������ ����������� ��� ������ � ��������� ��������
  THookRec = packed record
    Opcode: BYTE;
    Ofs: integer;
  end;

procedure WriteAtCode(P: POINTER; Buf: POINTER; Count: INTEGER);
var
  Temp: INTEGER;
begin
  VirtualProtect(P, Count, PAGE_READWRITE, @Temp);
  CopyMemory(P, Buf, Count);
  VirtualProtect(P, Count, Temp, nil);
end;

procedure HookCode(P: POINTER; NewAddr: POINTER; UseCall: BOOLEAN; PatchSize: INTEGER) overload;
var
  HookRec: THookRec;
  NopCount: INTEGER;
  NopBuf: string;
begin
  if UseCall then
  begin
    HookRec.Opcode:=C_OPCODE_CALL;
  end
  else
  begin
    HookRec.Opcode:=C_OPCODE_JUMP;
  end;
  HookRec.Ofs:=INTEGER(NewAddr)-INTEGER(P)-C_UNIHOOK_SIZE;
  writeAtCode(P, @HookRec, 5);
  NopCount:=PatchSize-5;
  if NopCount>0 then
  begin
    SetLength(NopBuf, NopCount);
    FillChar(NopBuf[1], NopCount, CHR($90));
    writeAtCode(Ptr(INTEGER(P)+5), @NopBuf[1], NopCount);
  end;
end;

procedure HookCode(P: POINTER; NewAddr: POINTER; UseCall: BOOLEAN) overload;
begin
  HookCode(P, NewAddr, UseCall, 5);
end;

procedure ChangeLineInFile(FileName, SearchSubLine, NewLine: string);
var
  Text: TStringList;
  i: integer;
  Stream: TStream;
  tmp: char;
begin
  Text := TStringList.Create;
  Text.LoadFromFile(FileName);
  I := 0;
  while i < Text.Count do
  begin
    if Pos(SearchSubLine, Text.Strings[i]) <> 0 then
    begin
      Text.Strings[i] := NewLine;
      Text.LineBreak := chr($0A);
      Stream := TFileStream.Create(FileName, fmCreate);
      Text.SaveToStream(Stream);
      Stream.Position := Stream.Size-1;
      tmp := chr($0D);
      Stream.Write(tmp, 1);
      Stream.Free;
      exit;
    end;
    inc(i)
  end;
end;

end.

