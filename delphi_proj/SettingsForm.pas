unit SettingsForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  MyUtils, WTReg;

type
  TfrmSettings = class(TForm)
    eChessTimeStart: TEdit;
    eChessTimeAdd: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure eChessTimeAddChange(Sender: TObject);
    procedure eChessTimeStartChange(Sender: TObject);
  private
  public
  end;

var
  frmSettings: TfrmSettings;

implementation

{$R *.dfm}

procedure TfrmSettings.eChessTimeAddChange(Sender: TObject);
var
  num:Integer;
begin
  if Length(eChessTimeAdd.Text) = 0 then Exit;
  num := StrToIntDef(eChessTimeAdd.Text, 500);
  WTReg.WriteVal('Chess Time Add', num);
  MyUtils.ChangeLineInFile('.\Data\s\script07.erm'
    , '$WT_PARAM_2'
    , '!!VRz-1:S^' + IntToStr(num) + '^; set time per turn to add ; $WT_PARAM_2 ');
end;

procedure TfrmSettings.eChessTimeStartChange(Sender: TObject);
var
  num:Integer;
begin
  if Length(eChessTimeStart.Text) = 0 then Exit;
  num := StrToIntDef(eChessTimeStart.Text, 4000);
  WTReg.WriteVal('Chess Time Start', num);
  MyUtils.ChangeLineInFile('.\Data\s\script07.erm'
    , '$WT_PARAM_1'
    , '!!VRz-1:S^' + IntToStr(num) + '^; set time per game ; $WT_PARAM_1 ');
end;

procedure TfrmSettings.FormCreate(Sender: TObject);
begin
  eChessTimeStart.Text := IntToStr(WTReg.ReadValInt('Chess Time Start'));
  eChessTimeAdd.Text := IntToStr(WTReg.ReadValInt('Chess Time Add'));
end;

end.
